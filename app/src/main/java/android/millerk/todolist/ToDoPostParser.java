package android.millerk.todolist;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;

public class ToDoPostParser {

    private static ToDoPostParser parser;

    public ArrayList<todoPosts> todoPosts;

    private ToDoPostParser(){
        todoPosts = new ArrayList<todoPosts>();
    }

    public static  ToDoPostParser getInstance(){
        if(parser == null){
            parser = new ToDoPostParser();
        }
        return parser;
    }

    public JSONObject parserInuputStream(InputStream inputStream) {
        BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        JSONObject jsonObject = null;

        String currentLine;

        try {
            while ((currentLine = streamReader.readLine()) != null) {
                stringBuilder.append(currentLine);
            }
            JSONTokener jsonTokener = new JSONTokener(stringBuilder.toString());
            jsonObject = new JSONObject(jsonTokener);
        } catch (IOException error) {
            Log.e("ToDoPostParser", "IOExecption (parserInputStream): " + error);
        } catch (JSONException error) {
            Log.e("ToDoPostParser", "JSONExecption (parserInputStream): " + error);
        }

        return jsonObject;
        //produces the string {"JSON": "Hello, World"}.
        //An object that has fields that should be used to make a JSONObject
        //A JSONObject is an unordered collection of name/value pairs.

    }

    public void readToDoFeed(JSONObject jsonObject){
        todoPosts.clear();
        try {
            JSONArray postData = jsonObject.getJSONObject("data").getJSONArray("children");

            for(int index = 0; index < postData.length(); index ++){
                JSONObject post = postData.getJSONObject(index).getJSONObject("data");

                String title = post.getString("title");
                String url = post.getString("url");

                todoPosts todoPost = new todoPosts(title, url);
                todoPosts.add(todoPost);

            }
            // for loop ^^^
        }
        catch(JSONException error){
            Log.e("ToDoPostParser", "JSONExecption (readRedditFeed): " + error);
        }

    }
}
