package android.millerk.todolist;


import java.util.ArrayList;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class RedditPostAdapter extends RecyclerView.Adapter<ToDoPostHolder>{

        private ArrayList<ToDoPost> todoPosts;
        private ActivityCallback activityCallback;

        public RedditPostAdapter(ActivityCallback activityCallback){
            todoPosts = ToDoPostParser.getInstance().todoPosts;
            this.activityCallback = activityCallback;
        }

        @Override
        public ToDoPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            return new ToDoPostHolder(view);
        }

        @Override
        public void onBindViewHolder(ToDoPostHolder holder, final int position) {
            holder.titleText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri todoUri = Uri.parse(todoPosts.get(position).url);
                    activityCallback.onPostSelected(todoUri);
                }
            });


            holder.titleText.setText(todoPosts.get(position).title);

        }

        @Override
        public int getItemCount() {
            return todoPosts.size();
        }

}
