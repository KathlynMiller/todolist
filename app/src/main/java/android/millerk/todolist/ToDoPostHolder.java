package android.millerk.todolist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class ToDoPostHolder extends RecyclerView.ViewHolder {

    public TextView titleText;
    public ToDoPostHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}
